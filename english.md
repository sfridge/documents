# Project sFridge

Project made by :

Buguel Maxime,
De-coster Romain,
Roeland Quentin

## Introduction

Sfridge is tool to manage the content of your fridge. To begin this tool, we made this tool to manage the beers suply but as we are ussing the openfoodfact.org API, Sfridge can be use with any kind of food or drinks that have a barcode.

## The project architecture

### * The backend

Even if we use Constellation, we have decided to make our own back-end using a PostgreSQL database and an API made with Ruby on Rails.


### * Frontend : a mobil app

We have made a mobil app with several features :

+ The visualisation of the content of the fridge ( where you can also add or remove items of it)

+ The barcode scanner that allow you to add new product to your fridge. To do so you can use the camera from your phone or simply enter the barcode with the keyboard. When you add a product to the fridge you can tell if it is an important product with the field "Must have". The field "Must have" is use to determine your shopping list. The product that you wants to add must be refferenced on openfoodfact.org, if its not you will have a notification telling you so that you can go their website and add it your self. For a better knowledge of your supply it is better to add item individually.

+ The shopping list, as described before let you know all the must have products that are not in stock anymore (quantity fell to 0). You can also add thoses products from this screen so you donc need to scan/type the barcode everytime you buy somme.


### * Constellation

As we said before, our application has it's own backend so it can be used without Constellation. But Constellation allow us to add a "brain" to Sfridge. We made several packages that are using the Sfridge API and the Pushbullet packages to notify the user(s) when some major changes are made in the fridge. For exemple if someone at home take a last "must have" item other users will get a Pushbullet and can bring some back home. Users will also be notify if the quantity get downto low so that you always have beers in the fridge.  

The minimum number of beer in the fridge is set with a default value but it can change with events like soccer game.            

## Installation

### * Backend

You can deploy the backend with docker
```bash
docker run -p 3000:3000 -e APP_USER_POSTGRES_PASSWORD=<password> --link <postgres>:postgres -d de-coster.fr:5000/sfridge
```

The api will be at this adress : <your_ip>:3000/api.

To insatall this without Docker :


You will need to have Ruby 2.2 min and Rails 4.2 min.
Then you will need to clone the repo with the backend and then use this command into the project folder.

```
#!bash

bundle install && rake db:create && rake db:migrate && rails server
```

### * Application

To use the application you will need to have the API deployed and to have ionic on your computer.
You will need to make a minor change in the  sources : you will have to change the adress of the api in the ```./www/js/service.js``` file.
Once you have change the URL of the API you can build the app with Ionic and intall it on your smartphone.

We are thinking of a way to change this process so that it will be easier to install the app.

      
### * Constellation

You can install the package SFridge on anny sentinel but prefer one that is always running, this package send different kinf of stateObject:

+ Content : the content of the fridge

+ Tobuy : the shopping list

+ Count : the number of item in the fridge

Parameters :

+ Interval : time between each call to the API

+ fridge_url : URL of the fridge end point

+ shopping_url : URL of the shopping list end point

There is also a web interface to visualize this information if you don't use the app.

If you like soccer the package MatchOfTheDay will let you know the next matches and also notify you with the content of the fridge. 

+ MOTD : incomming matches.

Parameters :

+ Interval : time between each call to the API

+ Team: name of your favorite team.

+ url : url of RSS flux you want to follow (ex : http://www.matchendirect.fr/rss/matchendirect.xml)

And last Mybrain that use all this informations and notify users with Pushbullet.

Parameters :

+ minBeer : minimum number of beers to notify the user

+ minBeerFootballGame : minimum number of beers to notify the user on a football game