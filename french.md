# Projet sFridge

Projet réalisé par :

Buguel Maxime,
De-coster Romain,
Roeland Quentin

## Introduction

Sfridge est un outil de gestion de frigidaire. Dans un premier temps nous nous sommes concentrés sur le gestion d'un stock de bičre bien que l'application soit compatible avec tous les aliments & boissons présents sur l'API du site de openfoodfact.org

## La structure du projet

### * Le backend

Malgré l'utilisation de Constellation, nous avons souhaité mettre en place une API Rest afin que Sfridge soit utilisable sans Constellation.

Cette api est développée en Ruby on rails avec différents endpoints. Cette api nous permet d'avoir un accčs facilité ŕ une base de donnée Postgresql 9.4. que ce soit depuis une application Mobile ou un Package Constellation.


### * L'application mobile

L'application mobile dispose de plusieurs fonctionnalités :

+ La visualisation du stock actuel (qui nous permet également de modifier les quantités des différents produit actuellement dans le frigidaire.

+ Le scanner qui nous permet d'ajouter de nouveaux produits ou des produits qui ne sont plus en stock. Pour se faire nous pouvons soit scanner un code-barre (ŕ l'aide de l'appareil photo) soit le rentrer ŕ la main et d'indiquer la quantité de ce produit ainsi que son importance (limportance permet de générer une liste de course : un produit important dont la quantité passe ŕ 0 arrive dans la liste de course). L'application fait appel a l'API openfoodfact.org afin de récupérer des informations supplémentaires comme le nom, la marque, et la quantité du produit scanné. Si un produit n'est pas trouvé, l'utilisateur peut se connecter ŕ openfoodfact.org afin d'ajouter le produit. Attention, pour mieux gérer les stocks il est préférable de scanner les objets individuellement plutôt que les code-barre présent sur les lots (pour un pack de bičre, il vaut mieux scanner une canette et indiquer la quantité pressente dans le pack)

+ La liste de course qui comme évoquée plutôt, permet de visualiser l'ensemble des produits "importants" qui ne sont plus présents dans notre frigidaire. On peut également ajuster les quantités depuis cet onglet afin de ne pas avoir ŕ scanner ces produits ŕ chaque fois.


### * Utilisation de Constellation

Comme évoqué précédemment nous avons une application qui peux ętre utilisé sans Constellation, mais nous avons développé différents packages constellation afin de facilité l'interaction entre l'utilisateur et Sfridge, et d'ajouter une "intelligence" a notre frigidaire connecté. En effet nos différents packages nous permettent de surtout de notifier les utilisateurs (via le package Pushbullet). Ces fonctionnalités sont utiles surtout lorsque l'on ne vit pas seul (famille, collocation etc...) : si quelqu'un ŕ la maison prend un dernier produit important les autres utilisateurs sont notifiés et peuvent en acheter avant de rentrer (du travail, ou de cours). Dans le cas de la gestion du stock de bičre, les utilisateurs sont prévenus lorsqu'un produit important, męme non manquant, atteint une quantité jugée comme trop faible.
            
Le nombre de bičre minimum est paramétrée via Constellation mais peut aussi évoluer en fonction des événements grâce ŕ la possibilité d'intéragir avec d'autres plug in. Par exemple, si l'utilisateur est fan de football, il peut recevoir une notification indiquant que le stock habituel risque de ne pas suffire, si un match est programmé.     

## Installation

### * Backend

L'api peut ętre déployée grâce ŕ docker
```bash
docker run -p 3000:3000 -e APP_USER_POSTGRES_PASSWORD=<password> --link <postgres>:postgres -d de-coster.fr:5000/sfridge
```

L'api est disponible ŕ l'adresse : <your_ip>:3000/api.

Installation sans docker :

Il faut avec ruby 2.2 min et rails 4.2 min, ensuite il faut clone le dépot du backend du projet et taper cette commande dans le dossier du projet:


```
#!bash

bundle install && rake db:create && rake db:migrate && rails server
```


### * Application

Affin d'interagir avec la base de donnée via l'API nous avons mis en place une application cross platforme réalisée a l'aide de Ionic Framework.
Pour pouvoir mettre en place SFridge chez vous, vous allez devoir installer la base de donnée ainsi que l'API sur le serveur de votre choix (comme précisé ci-dessus). Et pour pouvoir utiliser l'application, vous allez devoir récupéré les source de l'application et éditer le fichier ```./www/js/service.js``` afin d'indiquer l'URL de votre back-end.

Une fois le fichier ```service.js``` modifier il faudra utiliser Ionic pour créer les fichier dinstallation de l'application pour votre smartphone.

Nous réfléchissons a une solution afin d'indiquer l'URL directement depuis l'application afin de faciliter linstallation de celle-ci

      
### * Constellation


L'utilisateur peut installer le package SFridge sur la sentinelle de son choix. Celui-ci envoie deux types de stateObject : 

+ Content : le contenu du frigo

+ Tobuy : la liste de courses

+ Count : nombre total d'articles

Les paramčtres suivants seront ŕ spécifier ŕ l'installation :

+ Interval : définie l'interval de rafraichissement des données

+ fridge_url : défini le endpoint de l'api pour la gestion du frigo

+ shopping_url : défini le endpoint de l'api pour la gestion de la liste de courses.

Une interface web est fournie pour visualiser facilement ŕ un męme endroit ces informations. 

Si vous ętes fan de football, le package MatchOfTheDay vous permettra d'anticiper vos prochaines consommations et prévoir suffisamment de stocks. Sur n'importe quel type de sentinelle. 

+ MOTD : prochains matchs ŕ suivre

Ses paramčtres sont :

+ Interval : défini l'interval de rafraîchissement du programme des matchs ŕ venir.

+ Team: défini l'équipe Favorite

+ url : url du flux RSS fourni au format URL de votre source favorie (ex : http://www.matchendirect.fr/rss/matchendirect.xml)

Enfin, le package myBrain apporte l'intelligence et permet de notifier l'utilisateur en regroupant les informations émises par ces packages (l'installation du package PushBullet est nécessaire pour bénéficier des notifications).
Qui lui a pour paramètre :

+ minBeer : nombre minimum de bières avant de notifier l'utilisateur

+ minBeerFootballGame : nombre minimum de bières pour un soir de match avant de notifier l'utilisateur

